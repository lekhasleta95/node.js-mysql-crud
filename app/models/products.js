const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const Product = sequelize.define('product', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },
    title: Sequelize.STRING,
    calories: Sequelize.INTEGER,
    fat: Sequelize.INTEGER,
    carbs: Sequelize.INTEGER,
    protein: Sequelize.INTEGER,
})
module.exports = Product;