const express = require('express');


const cors = require('cors');
const bodyParser = require('body-parser');
const sequelize = require('./app/util/database');

const app = express();

const Product = require('./app/models/products');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
// parse application/json
app.use(bodyParser.json())

sequelize.sync().then(res => {
    app.listen(3000);
}).catch(err => {
    console.log(err);
})


app.get('/products', (req, res) => {
    Product.findAll().then(prod => {
        res.send(prod);
    }).catch(err => {
        console.log(err);
    });
});
app.post('/product', (req, res) => {
    let body = req.body;
    Product.create({
        title: body.title,
        calories: body.calories,
        fat: body.fat,
        carbs: body.carbs,
        protein: body.protein
    }).then(result => {
        res.send('Product added!')
    }).catch(err => {
        console.log(err);

    })
});
app.delete('/product/:id', (req, res) => {
    Product.destroy({ where: { id: req.params.id } }).then(result => {
        res.send('Deleted!');
    }).catch(err => {
        console.log(err);
    })
});
app.put('/product/:id', (req, res) => {
    Product.update({ ...req.body }, { where: { id: req.params.id } }).then(result => {
        res.send('Updated');
    }).catch(err => {
        console.log(err);
    })
});